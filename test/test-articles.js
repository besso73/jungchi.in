/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , should = require('should')
  , request = require('supertest')
  , app = require('../server')
  , context = describe
  , User = mongoose.model('User')
  , People = mongoose.model('People')
  , Article = mongoose.model('Article')
  , agent = request.agent(app)

var count

/**
 * Articles tests
 */

describe('Articles', function () {
  var people_id = '';

  before(function (done) {

    People.find().exec(function(err,res){
      people_id = res[res.length-1]._id
    })

    // create a user
    var user = new User({
      email: 'foobar@example.com',
      name: 'Foo bar',
      username: 'foobar',
      password: 'foobar'
    })
    user.save(done)

  })

  describe('GET /people/:pid/articles', function () {
    it('should respond with Content-Type text/html', function (done) {
      agent
      .get('/people/' + people_id +'/articles')
      .expect('Content-Type', /html/)
      .expect(200)
      .expect(/Articles/)
      .end(function(err,res){
          if(err) { throw err }
          done();
        })
    })
  })

  describe('GET /people/:pid/articles/new', function () {

    it('should redirect to /login', function (done) {
      agent
      .get('/people/'+ people_id +'/articles/new')
      .expect('Content-Type', /plain/)
      .expect(302)
      .expect('Location', '/login')
      .expect(/Moved Temporarily/)
      .end(function(err,res){
        if(err) { throw err }
        done();
      })
    })

    context('When logged in', function () {
      // TODO. Forbidden happen in /users/session, dig that
      it('login',function (done) {
        // login the user
        agent
        .post('/users/session')
        .field('email', 'foobar@example.com')
        .field('password', 'foobar')
        .end(done)
      })

      it('should respond with Content-Type text/html', function (done) {
        agent
        .get('/people/'+ people_id +'/articles/new')
        .expect('Content-Type', /html/)
        .expect(200)
        .expect(/New Article/)
        .end(done)
      })
    })

  })

  describe('POST /people/:pid/articles/new', function () {
    context('When not logged in', function () {
      it('should redirect to /login', function (done) {
        request(app)
        .get('/people/' + people_id + '/articles/new')
        .expect('Content-Type', /plain/)
        .expect(302)
        .expect('Location', '/login')
        .expect(/Moved Temporarily/)
        .end(done)
      })
    })

    context('When logged in', function () {
      before(function (done) {
        // login the user
        agent
        .post('/users/session')
        .field('email', 'foobar@example.com')
        .field('password', 'foobar')
        .end(done)
      })
      
      /*
      describe('Invalid parameters', function () {
        before(function (done) {
          Article.count(function (err, cnt) {
            count = cnt
            done()
          })
        })

        it('should respond with error', function (done) {
          agent
          .post('/people/'+people_id+'/articles')
          .field('title', '')
          .field('body', 'foo')
          .field('people',people_id)
          .expect('Content-Type', /html/)
          .expect(200)
          .expect(/Article title cannot be blank/)
          .end(done)
        })

        it('should not save to the database', function (done) {
          Article.count(function (err, cnt) {
            count.should.equal(cnt)
            done()
          })
        })
      })
      */
     
      describe('Valid parameters', function () {
        before(function (done) {
          Article.count(function (err, cnt) {
            count = cnt
            done()
          })
        })

        it('should redirect to the new article page', function (done) {
          agent
          .post('/people/'+people_id+'/articles')
          .field('title', 'foo')
          .field('body', 'bar')
          .field('people',people_id)
          .expect('Content-Type', /plain/)
          .expect('Location', /\/articles\//)
          .expect(302)
          .expect(/Moved Temporarily/)
          .end(done)
        })

        it('should insert a record to the database', function (done) {
          Article.count(function (err, cnt) {
            cnt.should.equal(count + 1)
            done()
          })
        })

        it('should save the article to the database', function (done) {
          Article
          .findOne({ title: 'foo'})
          .populate('user')
          .exec(function (err, article) {
            should.not.exist(err)
            article.should.be.an.instanceOf(Article)
            article.title.should.equal('foo')
            article.body.should.equal('bar')
            article.user.email.should.equal('foobar@example.com')
            article.user.name.should.equal('Foo bar')
            done()
          })
        })
      })

    })
  })

  // test button
  describe('plus button', function () {
    var article_id = '';
    before(function (done) {
      Article.find({}).exec(function(error,res){
        var count = res.length;
        article_id = res[count-1]._id
        done();
      })
    })
    console.log(article_id)

    it('should add +1 for Article, People and insert Button model', function (done) {
      agent
      .post('/button/add')
      .field('article_id', article_id)
      .expect('success')
      .end(done)
    })
  })



  /*
  after(function (done) {
    User.find({'username':'foobar'}).remove(function(error,result){
      done();
    })
  })
  */

  after(function (done) {
    require('./helper').clearDb(done)
  })

})