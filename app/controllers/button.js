/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  Button = mongoose.model('Button'),
  Article = mongoose.model('Article'),
  People = mongoose.model('People'),
  utils = require('../../lib/utils'),
  extend = require('util')._extend


/**
 * TODO. no exception.
 */
exports.add = function (req, res) {

  var user = req.user
  var aid = req.body.article_id

  Button.update({ user : user._id, article : aid },{$set:{ user : user._id, article : aid }},{upsert:true},function(err, numberAffected, raw) {
    if (err) return res.end('error')    
  })

  Article.update({ _id : aid }, { $inc: {"button" : 1 } }, function(error, result) {
	  if (error) return res.end('error')
  })

  var people_id = '';
  Article.find({_id:aid},{people:1}).exec(function(error,result) {
    people_id = result[0].people;
    People.update({ _id : people_id }, { $inc: {"button" : 1 } }, function(error, result) {
      if (error) return res.end('error')
      res.end('success')
    })
  });
}

