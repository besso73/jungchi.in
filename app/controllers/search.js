/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , Article = mongoose.model('Article')

/**
 * List items searchged with a search
 */
/* TODO. i did search targetting only for Article model ( title, body, tags )
 * To use multiple collection, we need to use npm modules like text search or else
 */
exports.index = function (req, res) {
  var re = new RegExp( req.param('search'), 'i' );
  var criteria = {
  					$or:[ 
  					   {'title' : { $regex : re} },
	  				   { 'body':  { $regex : re} },
	  				   { 'tags':  { $regex : re} }
	  				]
				 }
  var perPage = 5
  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1
  var options = {
    perPage: perPage,
    page: page,
    criteria: criteria
  }
  Article.list(options, function(err, articles) {
    if (err) return res.render('500')
    Article.count(criteria).exec(function (err, count) {
      res.render('articles/index', {
        title: 'Articles searchged by "' + req.param('search') + '"',
        articles: articles,
        page: page + 1,
        pages: Math.ceil(count / perPage)
      })
    })
  })
}
