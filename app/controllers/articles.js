/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , People = mongoose.model('People')
  , Article = mongoose.model('Article')
  , utils = require('../../lib/utils')
  , extend = require('util')._extend

/**
 * Load
 */

exports.load = function(req, res, next, id){
  var User = mongoose.model('User')

  Article.load(id, function (err, article) {
    if (err) return next(err)
    if (!article) return next(new Error('not found'))
    req.article = article
    next()
  })
}

/**
 * List
 */
// TODO. article list always be under ppl, besso
exports.index = function(req, res){
  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1
  var perPage = 30
  var options = {
    perPage: perPage,
    page: page
  }

  Article.list(options, function(err, articles) {
    if (err) return res.render('500')
    Article.count().exec(function (err, count) {
      res.render('articles/index', {
        title: 'Articles',
        articles: articles,
        page: page + 1,
        pages: Math.ceil(count / perPage)
      })
    })
  })
}

/**
 * New article
 */

exports.new = function(req, res){
  res.render('articles/new', {
    title: 'New Article for ' + req.people.title,
    article: new Article({}),
    people : req.people
  })
}

/**
 * Create an article
 */

exports.create = function (req, res) {
  var article = new Article(req.body)

  article.user = req.user

  article.uploadAndSave(req.files.image, function (err) {
    if (!err) {
      req.flash('success', 'Successfully created article!')
      return res.redirect('/people/'+ article.people +'/articles/'+article._id)
    }

    res.render('articles/new', {
      title: '새 글쓰기',
      article: article,
      errors: utils.errors(err.errors || err)
    })
  })
}

/**
 * Edit an article
 */

exports.edit = function (req, res) {
console.log(req)
  res.render('articles/edit', {
    title: req.article.title,
    article: req.article,
    people: req.people
  })
}

/**
 * Update article
 */

exports.update = function(req, res){
  var article = req.article
  article = extend(article, req.body)

  article.uploadAndSave(req.files.image, function(err) {
    if (!err) {
      return res.redirect('/people/'+article.people+'/articles/' + article._id)
    }

    res.render('articles/edit', {
      title: '글 수정하기',
      article: article,
      errors: err.errors
    })
  })
}

/**
 * Show
 */

exports.show = function(req, res){
  //console.log(req.user)
  var isAuthor = false;
  if ( typeof(req.user) !== 'undefined' ) {
    console.log(req.user)
    if (req.user.id == req.article.user._id) {
      isAuthor = true;
    }
  }
  res.render('articles/show', {
    title: req.article.title,
    people : req.people,
    article: req.article,
    isAuthor: isAuthor
  })
}

/**
 * Delete an article
 */

exports.destroy = function(req, res){
  var article = req.article
  article.remove(function(err){
    req.flash('info', 'Deleted successfully')
    res.redirect('/articles')
  })
}
