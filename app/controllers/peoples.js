/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , People = mongoose.model('People')
  , Article = mongoose.model('Article')
  , utils = require('../../lib/utils')
  , extend = require('util')._extend

/**
 * Load
 */
exports.load = function(req, res, next, id){
  var User = mongoose.model('User')

  People.load(id, function (err, people) {
    if (err) return next(err)
    if (!people) return next(new Error('not found'))
    req.people = people
    next()
  })
}

/**
 * List
 */

exports.index = function(req, res){

  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1
  var perPage = 5
  var options = {
    perPage: perPage,
    page: page
  }

  Article.list(options, function(err, articles) {
    if (err) return res.render('500')
    Article.count().exec(function (err, count) {
      res.render('peoples/index', {
        title: 'Most Popular Articles',
        articles: articles,
        page: page + 1,
        pages: Math.ceil(count / perPage),
        utils: utils
      })
    })
  })

  /*
  People.list(options, function(err, peoples) {
    if (err) return res.render('500')
    People.count().exec(function (err, count) {
      res.render('peoples/index', {
        title: 'People',
        peoples: peoples,
        page: page + 1,
        pages: Math.ceil(count / perPage)
      })
    })
  })
  */
}

/**
 * New People
 */

exports.new = function(req, res){
  res.render('peoples/new', {
    title: '새 인물 글쓰기',
    people: new People({})
  })
}

/**
 * Create an People
 */

exports.create = function (req, res) {
  var people = new People(req.body)
  people.user = req.user

  people.uploadAndSave(req.files.image, function (err) {
    if (!err) {
      req.flash('success', 'Successfully created people!')
      return res.redirect('/people/'+people._id)
    }

    res.render('peoples/new', {
      title: 'New People',
      article: people,
      errors: utils.errors(err.errors || err)
    })
  })
}

/**
 * Edit an people
 */

exports.edit = function (req, res) {
  res.render('peoples/edit', {
    title: req.people.title,
    people: req.people
  })
}

/**
 * Update people
 */

exports.update = function(req, res){
  var people = req.people
  people = extend(people, req.body)

  people.uploadAndSave(req.files.image, function(err) {
    if (!err) {
      return res.redirect('/people/' + people._id)
    }

    res.render('peoples/edit', {
      title: '인물 정보 수정하기',
      people: people,
      errors: err.errors
    })
  })
}

/**
 * Show
 */

exports.show = function(req, res){
  /*
  res.render('peoples/show', {
    title: req.people.title
    article: req.article
  })
  */

  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1
  var perPage = 30
  var options = {
    perPage: perPage,
    page: page,
    people: req.people.id
  }

  Article.list(options, function(err, articles) {
    if (err) return res.render('500')
    Article.count({people:req.people.id}).exec(function (err, count) {
      //console.log( count );
      res.render('peoples/show', {
        title: req.people.title,
        people: req.people,
        articles: articles,
        page: page + 1,
        pages: Math.ceil(count / perPage),
        utils: utils
      })
    })
  })
}

/**
 * Delete an people
 */

exports.destroy = function(req, res){
  var people = req.people
  people.remove(function(err){
    req.flash('info', 'Deleted successfully')
    res.redirect('/people')
  })
}

exports.upload_file = function(req,res) {
  var people = new People(req.body)
  people.user = req.user

  people.upload_file(req.files.image, function (err) {
    if (!err) {
      req.flash('success', 'Successfully created people!')
      return res.redirect('/people/'+people._id)
    }

    res.render('peoples/new', {
      title: 'New People',
      article: people,
      errors: utils.errors(err.errors || err)
    })
  })
}