/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , Imager = require('imager')
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env]
  , imagerConfig = require(config.root + '/config/imager.js')
  , Schema = mongoose.Schema
  , utils = require('../../lib/utils')

/**
 * Getters
 */

var getTags = function (tags) {
  return tags.join(',')
}

/**
 * Setters
 */

var setTags = function (tags) {
  return tags.split(',')
}

/**
 * People Schema
 */

var PeopleSchema = new Schema({
  name: {type : String, default : '', trim : true},
  title: {type : String, default : '', trim : true},
  body: {type : String, default : '', trim : true},
  user: {type : Schema.ObjectId, ref : 'User'},
  tags: {type: [], get: getTags, set: setTags},
  image: {
    cdnUri: String,
    files: []
  },
  button: {type: Number, default : 0},
  createdAt  : {type : Date, default : Date.now}
})

/**
 * Validations
 */

PeopleSchema.path('title').required(true, 'People title cannot be blank');
PeopleSchema.path('body').required(true, 'People body cannot be blank');

/**
 * Pre-remove hook
 */

PeopleSchema.pre('remove', function (next) {
  var imager = new Imager(imagerConfig, 'S3')
  var files = this.image.files

  // if there are files associated with the item, remove from the cloud too
  imager.remove(files, function (err) {
    if (err) return next(err)
  }, 'people')

  next()
})

/**
 * Methods
 */

PeopleSchema.methods = {

  /**
   * Save people and upload image
   *
   * @param {Object} images
   * @param {Function} cb
   * @api private
   */

  uploadAndSave: function (images, cb) {
//console.log(images);
//console.log(images.length);
//console.log(images.size);
    if (!images || !images.length) return this.save(cb)

    //var imager = new Imager(imagerConfig, 'S3')
    var imager = new Imager(imagerConfig, 'Local')
    var self = this

    imager.upload(images, function (err, cdnUri, files) {
      if (err) return cb(err)
      if (files.length) {
        self.image = { cdnUri : cdnUri, files : files }
      }
      self.save(cb)
    }, 'people')
  },

  /**
   * Add comment
   *
   * @param {User} user
   * @param {Object} comment
   * @param {Function} cb
   * @api private
   */

  addComment: function (user, comment, cb) {
    var notify = require('../mailer')

    this.comments.push({
      body: comment.body,
      user: user._id
    })

    if (!this.user.email) this.user.email = 'email@product.com'
    notify.comment({
      people: this,
      currentUser: user,
      comment: comment.body
    })

    this.save(cb)
  },

  /**
   * Remove comment
   *
   * @param {commentId} String
   * @param {Function} cb
   * @api private
   */

  removeComment: function (commentId, cb) {
    var index = utils.indexof(this.comments, { id: commentId })
    if (~index) this.comments.splice(index, 1)
    else return cb('not found')
    this.save(cb)
  },

  upload_file : function(req, res) {

    var fs = require('fs');
    console.log(req.files);

    fs.readFile(req.files.upload.path, function (err, data) {
      var newPath = 'assets/files/' + req.files.upload.name;
        fs.writeFile(newPath, data, function (err) {
        if (err) res.view({err: err});
          html = "";
          html += "<script type='text/javascript'>";
          html += "    var funcNum = " + req.query.CKEditorFuncNum + ";";
          html += "    var url     = \"/files/" + req.files.upload.name + "\";";
          html += "    var message = \"Uploaded file successfully\";";
          html += "";
          html += "    window.parent.CKEDITOR.tools.callFunction(funcNum, url, message);";
          html += "</script>";

          res.send(html);
      });

    });
}
}

/**
 * Statics
 */

PeopleSchema.statics = {

  /**
   * Find people by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api private
   */

  load: function (id, cb) {
    this.findOne({ _id : id })
      .populate('user', 'name email username')
      .populate('comments.user')
      .exec(cb)
  },

  /**
   * List peoples
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

  list: function (options, cb) {
    var criteria = options.criteria || {}

    this.find(criteria)
      .populate('user', 'name username')
      .sort({'createdAt': -1}) // sort by date
      .limit(options.perPage)
      .skip(options.perPage * options.page)
      .exec(cb)
  }

}

mongoose.model('People', PeopleSchema)
