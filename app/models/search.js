/**
 * Module dependencies.
 */
/**
 * Module dependencies.
 */
var mongoose = require('mongoose')
  , Article = mongoose.model('Article')
  , People = mongoose.model('People')
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env]
  , Schema = mongoose.Schema
  , utils = require('../../lib/utils')

/**
 * Getters
 */

var getTags = function (tags) {
  return tags.join(',')
}

/**
 * Setters
 */

var setTags = function (tags) {
  return tags.split(',')
}

// TODO. how to define existing Schema, besso
var SearchSchema = new Schema(
  { id:{type : Schema.ObjectId}, title: String, body: String, tags: String },
  { collection: 'article' },
  { collection: 'people' }
)

mongoose.model('Search', SearchSchema)