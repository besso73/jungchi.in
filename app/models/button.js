/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env]
  , Schema = mongoose.Schema
  , utils = require('../../lib/utils')


/**
 * Button Schema
 */

var ButtonSchema = new Schema({
  user: {type : Schema.ObjectId, ref : 'User'},
  article: {type : Schema.ObjectId, ref : 'Article'},
  createdAt  : {type : Date, default : Date.now}
})

mongoose.model('Button', ButtonSchema)