## Issues 를 확인하세요. 원하는 작업 Ticket을 생성하여 중복작업이 없도록 합니다.

## 기본 코드는 github 의 nodejs express jade mongo 를 이용한 간단한 Board Demo 를 이용했습니다.
Read the [wiki](https://github.com/madhums/node-express-mongoose/wiki)

## Install

**NOTE:** node 와 npm 을 먼저 설치하셔야 합니다. "node": "0.10.x" node 버젼이 0.11.x 에서 npm module 들이 제대로 설치되지 않았으니 주의하세요

**NOTE:** mongodb 도 따로 설치해야 할수도 있습니다. sudo apt-get install mongodb (ubuntu)
```sh
  $ git clone git@bitbucket.org:besso73/jungchi.in.git
  $ cd jungchi.in
  $ npm install
  $ npm start
```

**NOTE:** 필요한 credentials : facebook, twitter, github APP_ID and APP_SECRET in `config/config.js`. 

**NOTE:** 이미지 업로드를 위해 S3 credentials 가 있으면 좋습니다. Rackspace keys in `config/imager.js`.

**NOTE:** async 는 버젼 0.1.22 로 해야 dependency 이슈가 없었습니다. imager::package.json 에 업버젼 정의 된듯 합니다. dependency 이슈는 사알살 잘 .. 굿럭
Then visit http://{hostname}:3000/

## 디비 dump and restore
```
mongodump --db dev
mongorestore --db dev --collection users dump/dev/users.bson
mongorestore --db dev --collection sessions dump/dev/sessions.bson
mongorestore --db dev --collection peoples dump/dev/peoples.bson
mongorestore --db dev --collection articles dump/dev/articles.bson
mongorestore --db dev --collection buttons dump/dev/buttons.bson
```

## 디렉토리 구조. 심플합니다 :)
```
-app/
  |__controllers/
  |__models/
  |__mailer/
  |__views/
-config/
  |__routes.js
  |__config.js
  |__passport.js (auth config)
  |__imager.js (imager config)
  |__express.js (express.js configs)
  |__middlewares/ (custom middlewares)
-public/
```

## Tests : test 폴더아래가 개발시작점이 되면 좋습니다.

```sh
$ npm test
```