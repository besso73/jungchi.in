$(document).ready(function () {

  var CSRF_HEADER = 'X-CSRF-Token';
  
  var setCSRFToken = function(securityToken) {
    jQuery.ajaxPrefilter(function(options, _, xhr) {
      if ( !xhr.crossDomain ) 
          xhr.setRequestHeader(CSRF_HEADER, securityToken);
    });
  };
  
  setCSRFToken($('meta[name="csrf-token"]').attr('content'));

/*
  $('#tags').tagsInput({
    'height':'60px',
    'width':'280px'
  });
*/

  $('#search').bind('keyup',function(e){
  	if (e.which === 13) {
      var param = $(this).val();
      /* TODO. atc later
      $.get( '/search',parameters, function(data) {
        $('#results').html(data);
      });
      */
      param = encodeURIComponent(param);
      var url = '/search/' + param;
      location.href = url;
  	}
  });

  $('button#hate').bind('click',function(e){
  	var article_id = $(this).data('article_id');
  	var data = {
  	  'article_id' : article_id
  	};
  	/*
    $.post( '/button/add', data, function(data) {
      debugger;
      //$this.text(data);
    });
  	*/

    $.ajax({
      url: '/button/add',
      type: 'POST',
      cache: false,
      data: data,
      success: function(res){
        if(res == 'login') {
          location.href = '/login';
        }
      },
      error: function(jqXHR, textStatus, err){
        alert('text status ' + textStatus + ', err '+err)
      }
    })

  });

});