/*
 *  Generic require login routing middleware
 */

exports.requiresLogin = function (req, res, next) {
  if (req.isAuthenticated()) return next()
  if (req.method == 'GET') req.session.returnTo = req.originalUrl
  res.redirect('/login')
}

/*
 *  User authorization routing middleware
 */

exports.user = {
  hasAuthorization: function (req, res, next) {
    if (req.profile.id != req.user.id) {
      req.flash('info', 'You are not authorized')
      return res.redirect('/users/' + req.profile.id)
    }
    next()
  }
}

/*
 *  People authorization routing middleware
 */

exports.people = {
  hasAuthorization: function (req, res, next) {
    if (req.people.user.id != req.user.id) {
      req.flash('info', 'You are not authorized')
      return res.redirect('/peoples/' + req.people.id)
    }
    next()
  }
}

/*
 *  Article authorization routing middleware
 */

exports.article = {
  hasAuthorization: function (req, res, next) {
    if (req.article.user.id != req.user.id) {
      req.flash('info', 'You are not authorized')
      return res.redirect('/articles/' + req.article.id)
    }
    next()
  }
}

/**
 * Comment authorization routing middleware
 */

exports.comment = {
  hasAuthorization: function (req, res, next) {
    // if the current user is comment owner or article owner
    // give them authority to delete
    if (req.user.id === req.comment.user.id || req.user.id === req.article.user.id) {
      next()
    } else {
      req.flash('info', 'You are not authorized')
      res.redirect('/articles/' + req.article.id)
    }
  }
}

/*
 *  Button authorization routing middleware
 */

exports.button = {
  hasAuthorization: function (req, res, next) {
    if (req.isAuthenticated()) return next()
    //if (req.method == 'POST') req.session.returnTo = req.originalUrl
    res.end('login')
    //res.redirect('/login')
    //if (req.button.user.id != req.user.id) {
    //  req.flash('info', 'You are not authorized')
    //  return res.redirect('/login')
    //}
    //next()
  }
}

