/*!
 * Module dependencies.
 */

var async = require('async')

/**
 * Controllers
 */

var users = require('../app/controllers/users')
  , peoples = require('../app/controllers/peoples')
  , articles = require('../app/controllers/articles')
  , auth = require('./middlewares/authorization')


/**
 * Route middlewares
 */

var articleAuth = [auth.requiresLogin, auth.article.hasAuthorization]
var peopleAuth  = [auth.requiresLogin, auth.people.hasAuthorization]
var commentAuth = [auth.requiresLogin, auth.comment.hasAuthorization]
var buttonAuth = [auth.button.hasAuthorization]

/**
 * Expose routes
 */

module.exports = function (app, passport) {

  // user routes
  app.get('/login', users.login)
  app.get('/signup', users.signup)
  app.get('/logout', users.logout)
  app.post('/users', users.create)
  app.post('/users/session',
    passport.authenticate('local', {
      failureRedirect: '/login',
      failureFlash: 'Invalid email or password.'
    }), users.session)
  app.get('/users/:userId', users.show)
  app.get('/auth/facebook',
    passport.authenticate('facebook', {
      scope: [ 'email', 'user_about_me'],
      failureRedirect: '/login'
    }), users.signin)
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: '/login'
    }), users.authCallback)
  app.get('/auth/github',
    passport.authenticate('github', {
      failureRedirect: '/login'
    }), users.signin)
  app.get('/auth/github/callback',
    passport.authenticate('github', {
      failureRedirect: '/login'
    }), users.authCallback)
  app.get('/auth/twitter',
    passport.authenticate('twitter', {
      failureRedirect: '/login'
    }), users.signin)
  app.get('/auth/twitter/callback',
    passport.authenticate('twitter', {
      failureRedirect: '/login'
    }), users.authCallback)
  app.get('/auth/google',
    passport.authenticate('google', {
      failureRedirect: '/login',
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
      ]
    }), users.signin)
  app.get('/auth/google/callback',
    passport.authenticate('google', {
      failureRedirect: '/login'
    }), users.authCallback)
  app.get('/auth/linkedin',
    passport.authenticate('linkedin', {
      failureRedirect: '/login',
      scope: [
        'r_emailaddress'
      ]
    }), users.signin)
  app.get('/auth/linkedin/callback',
    passport.authenticate('linkedin', {
      failureRedirect: '/login'
    }), users.authCallback)

  app.param('userId', users.user)

  // people routes
  app.param('pid', peoples.load)
  app.get('/people', peoples.index)
  app.get('/people/new', auth.requiresLogin, peoples.new)
  app.post('/people', auth.requiresLogin, peoples.create)
  app.get('/people/:pid', peoples.show)
  app.get('/people/:pid/edit', peopleAuth, peoples.edit)
  app.put('/people/:pid', peopleAuth, peoples.update)
  app.del('/people/:pid', peopleAuth, peoples.destroy)

  app.post('/uploader',peoples.upload_file)

  // article routes
  app.param('id', articles.load)
  app.get('/people/:pid/articles', articles.index)
  //app.get('/articles/new', auth.requiresLogin, articles.new)
  app.get('/people/:pid/articles/new', auth.requiresLogin, articles.new)
  app.post('/people/:pid/articles', auth.requiresLogin, articles.create)
  app.get('/people/:pid/articles/:id', articles.show)
  app.get('/people/:pid/articles/:id/edit', articleAuth, articles.edit)
  app.put('/people/:pid/articles/:id', articleAuth, articles.update)
  app.del('/people/:pid/articles/:id', articleAuth, articles.destroy)

  // home route
  app.get('/', peoples.index)

  // comment routes
  var comments = require('../app/controllers/comments')
  app.param('commentId', comments.load)
  app.post('/articles/:id/comments', auth.requiresLogin, comments.create)
  app.get('/articles/:id/comments', auth.requiresLogin, comments.create)
  app.del('/articles/:id/comments/:commentId', commentAuth, comments.destroy)


  // button route
  var button = require('../app/controllers/button')
  //app.post('/button/add', auth.requiresLogin, button.add)
  app.post('/button/add', buttonAuth, button.add)

  // about
  var introduction = require('../app/controllers/introduction')
console.log(introduction.about)  
  app.get('/intro', introduction.about)

  // tag routes
  var tags = require('../app/controllers/tags')
  app.get('/tags/:tag', tags.index)

  // search routes
  var search = require('../app/controllers/search')
  app.get('/search/:search', search.index);
}
