var path = require('path')
  , rootPath = path.normalize(__dirname + '/..')
  , templatePath = path.normalize(__dirname + '/../app/mailer/templates')
  , notifier = {
      service: 'postmark',
      APN: false,
      email: false, // true
      actions: ['comment'],
      tplPath: templatePath,
      key: 'POSTMARK_KEY',
      parseAppId: 'PARSE_APP_ID',
      parseApiKey: 'PARSE_MASTER_KEY'
    }

module.exports = {
  development: {
    db: 'mongodb://localhost/dev',
    root: rootPath,
    notifier: notifier,
    app: {
      name: 'Dev env'
    },
    // https://developers.facebook.com/x/apps/232482666875678/dashboard/
    facebook: {
      clientID: "232482666875678",
      clientSecret: "f245e523f1ce514b831b8a9b7d46a33e",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/facebook/callback"
    },
    // https://dev.twitter.com/apps/335684/show ( longin with screen name )
    twitter: {
      clientID: "nPoR8n11FkZuS5JMX6elRQ",
      clientSecret: "eQmE2wsZUkhbn38ks8QWIPkCNMvfzgi67U0xDDE",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/twitter/callback"
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://jjsoftwarehouse.com:7000/auth/github/callback'
    },
    google: {
      clientID: "APP_ID",
      clientSecret: "APP_SECRET",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/google/callback"
    },
    linkedin: {
      clientID: "CONSUMER_KEY",
      clientSecret: "CONSUMER_SECRET",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/linkedin/callback"
    }
  },
  test: {
    db: 'mongodb://jjsoftwarehouse.com/test',
    root: rootPath,
    notifier: notifier,
    app: {
      name: 'Test env'
    },
    facebook: {
      clientID: "APP_ID",
      clientSecret: "APP_SECRET",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/facebook/callback"
    },
    twitter: {
      clientID: "CONSUMER_KEY",
      clientSecret: "CONSUMER_SECRET",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/twitter/callback"
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://jjsoftwarehouse.com:7000/auth/github/callback'
    },
    google: {
      clientID: "APP_ID",
      clientSecret: "APP_SECRET",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/google/callback"
    },
    linkedin: {
      clientID: "CONSUMER_KEY",
      clientSecret: "CONSUMER_SECRET",
      callbackURL: "http://jjsoftwarehouse.com:7000/auth/linkedin/callback"
    }
  },
  production: {}
}
